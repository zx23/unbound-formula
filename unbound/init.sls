{%- from 'unbound/map.jinja' import unbound with context %}

{%- if unbound.anti_ad %}
include:
  - .anti_ad
{%- endif %}

{%- if salt.pillar.get('unbound.lookup.remote-control.control-enable', False) == 'yes' %}
unbound-control-setup:
  cmd.run:
    - name: unbound-control-setup
    - unless: 'test -f {{ unbound.lookup.remote-control.control-key-file }}'
{%- endif %}

unbound:
  pkg.installed:
    - name: {{ unbound.package }}
  service.running:
    - name: {{ unbound.service }}
    - enable: True
    - require:
      - pkg: unbound
      - file: unbound
  file.managed:
    - name: {{ unbound.config }}
    - source: salt://unbound/files/unbound.conf.jinja
    - context:
        settings: {{ unbound }}
    - template: jinja
    - user: root
    - group: {{ unbound.wheel_group }}
    - mode: '0644'
    - require:
      - pkg: unbound
  cmd.wait:
    - name: 'service unbound reload'
    - user: root
    - watch:
      - pkg: unbound
      - file: unbound
